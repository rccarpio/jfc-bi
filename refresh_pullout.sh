#!/bin/bash
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

echo "Refreshing Pullout"
echo ""

#test
touch /var/lib/mysql-files/test.txt

#get dates
today=`date --date="yesterday" +%Y-%m-%d`
yest=`date --date="2 days ago" +%Y-%m-%d`
datestamp=`date +%Y-%m-%d`

#pullout_tabular
echo "Downloading pullout_tabular data..."
mysql -uroot -pStarbucks jfc_production -e "select YEAR(tot.business_date) as year, MONTH(tot.business_date) as month, tot.business_date as date, IF(b.account_id = 3,'LUZ',IF(b.account_id = 4,'VIS',IF(b.account_id = 5,'MIN',''))) as region, dv.name as district, b.code as code, b.short_name as short_name, tot.amount as total_sales, tot.quantity as total_quantity, br.amount as bread_sales, br.quantity as bread_quantity, po_br.amount as bread_pullout, po_br.quantity as bread_pullout_q, po_br.amount/tot.amount as bread_pullout_to_total, po_br.quantity/tot.quantity as bread_pullout_to_total_q, po_br.amount/br.amount as bread_pullout_to_bread, po_br.quantity/br.quantity as bread_pullout_to_bread_q, nbr.amount as nonbread_sales, nbr.quantity as nonbread_quantity, ifnull(po_nbr.amount,0) as nonbread_pullout, ifnull(po_nbr.quantity,0) as nonbread_pullout_q, ifnull(po_nbr.amount,0)/tot.amount as nonbread_pullout_to_total, ifnull(po_nbr.quantity,0)/tot.quantity as nonbread_pullout_to_total_q, ifnull(po_nbr.amount,0)/nbr.amount as nonbread_pullout_to_nonbread, ifnull(po_nbr.quantity,0)/nbr.quantity as nonbread_pullout_to_nonbread_q from (select sum(i.amount) as amount, sum(i.quantity) as quantity, i.branch_id, i.business_date from invoices i use index (index_invoices_on_branch_id_and_updated_at_and_status) where i.updated_at >= '$yest 16:00:00' and i.updated_at < '$today 16:00:00' and i.status = 'S' group by i.branch_id, i.business_date)tot left join (select sum(d.amount) as amount, sum(d.quantity) as quantity, d.branch_id, date(addtime(d.document_date, '08:00:00')) as document_date from documents d where d.updated_at >= '$yest 16:00:00' and d.updated_at < '$today 16:00:00' and d.status = 'S' and ( d.document_purpose_id = 25 or d.document_purpose_id = 66 ) group by d.branch_id, date(addtime(d.document_date, '08:00:00')))po_tot on po_tot.document_date = tot.business_date and po_tot.branch_id = tot.branch_id inner join branches b on b.id = tot.branch_id inner join divisions dv on b.division_id = dv.id left join ( select sum(il.subtotal) as amount, sum(il.quantity) as quantity, i.branch_id, i.business_date from invoices i use index (index_invoices_on_branch_id_and_updated_at_and_status) inner join invoice_lines il on i.id = il.invoice_id inner join products p on p.id = il.product_id inner join categories c on c.id = p.category_id where i.updated_at >= '$yest 16:00:00' and i.updated_at < '$today 16:00:00' and i.status = 'S' and c.id in ( select id from categories where name = 'BREAD' ) group by i.branch_id, i.business_date)br on tot.branch_id = br.branch_id and tot.business_date = br.business_date left join ( select sum(il.subtotal) as amount, sum(il.quantity) as quantity, i.branch_id, i.business_date from invoices i use index (index_invoices_on_branch_id_and_updated_at_and_status) inner join invoice_lines il on i.id = il.invoice_id inner join products p on p.id = il.product_id inner join categories c on c.id = p.category_id where i.updated_at >= '$yest 16:00:00' and i.updated_at < '$today 16:00:00' and i.status = 'S' and c.id not in ( select id from categories where name = 'BREAD' ) group by i.branch_id, i.business_date)nbr on tot.branch_id = nbr.branch_id and tot.business_date = nbr.business_date left join ( select sum(dl.subtotal) as amount, sum(dl.quantity) as quantity, d.branch_id, date(addtime(d.document_date, '08:00:00')) as document_date from documents d inner join document_lines dl on d.id = dl.document_id inner join products p on p.id = dl.product_id inner join categories c on c.id = p.category_id where d.updated_at >= '$yest 16:00:00' and d.updated_at < '$today 16:00:00' and d.status = 'S' and c.id in ( select id from categories where name = 'BREAD' ) and ( d.document_purpose_id = 25 or d.document_purpose_id = 66 ) group by d.branch_id, date(addtime(d.document_date, '08:00:00')))po_br on tot.branch_id = po_br.branch_id and tot.business_date = po_br.document_date left join ( select sum(dl.subtotal) as amount, sum(dl.quantity) as quantity, d.branch_id, date(addtime(d.document_date, '08:00:00')) as document_date from documents d inner join document_lines dl on d.id = dl.document_id inner join products p on p.id = dl.product_id inner join categories c on c.id = p.category_id where d.updated_at >= '$yest 16:00:00' and d.updated_at < '$today 16:00:00' and d.status = 'S' and c.id not in ( select id from categories where name = 'BREAD' ) and ( d.document_purpose_id = 25 or d.document_purpose_id = 66 ) group by d.branch_id, date(addtime(d.document_date, '08:00:00')))po_nbr on tot.branch_id = po_nbr.branch_id and tot.business_date = po_nbr.document_date INTO outfile '/var/lib/mysql-files/pullout_tabular.tsv' fields terminated by '\t' lines terminated by '\n'; "
echo "pullout_tabular downloaded!"
echo ""

#go to var lib
cd /var/lib/mysql-files

#move files to shared/bi
echo "Moving files..."
mv pullout_tabular.tsv /home/vjavier/exports/shared/bi/newbi
echo "Files moved to /home/vjavier/exports/shared/bi/newbi"
echo ""

#compress the files
echo "Compressing files..."
cd /home/vjavier/exports/shared/bi/newbi
gzip pullout_tabular.tsv
echo "Files compressed!"
echo ""

#rename the files with the datestamp
echo "Renaming files..."
mv pullout_tabular.tsv.gz pullout_tabular-$datestamp.tsv.gz
echo "Files renamed!"
echo ""