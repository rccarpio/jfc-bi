#!/bin/bash
source /home/vjavier/.rvm/environments/ruby-2.3.3
cd /home/vjavier/scripts/newbi/
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
rvm use ruby-2.3.3

#aws sdk shiz
export AWS_ACCESS_KEY_ID=AKIAISWW45RZ5XGO3AQQ
export AWS_SECRET_ACCESS_KEY=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
export AWS_REGION=ap-southeast-1

#delete files from buckets with YTD and MTD comparisons
cd /home/vjavier/scripts/newbi/
ruby clear_buckets.rb

#run sales refresh script
/home/vjavier/scripts/newbi/refresh_sales.sh

#run pareto refresh script
/home/vjavier/scripts/newbi/refresh_pareto.sh

#run profit margin refresh script
/home/vjavier/scripts/newbi/refresh_pm.sh

#run transaction count refresh script
/home/vjavier/scripts/newbi/refresh_tc.sh

#run average check refresh script
/home/vjavier/scripts/newbi/refresh_ac.sh

#run pullout refresh script
/home/vjavier/scripts/newbi/refresh_pullout.sh

#put files to s3
cd /home/vjavier/exports/shared/bi/newbi
ruby refresh_bi_files.rb
