#!/bin/bash
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

echo "Refreshing Growth Rate"
echo ""

# current date
#cur_date=$(date +%Y-%m-%d)

# date mom
#prev_day=$(date -d "$cur_date -1 days" +%Y-%m-%d)
#fd_month=$(date -d "$prev_day" +%Y-%m-01)
#jan_month=$(date -d "$prev_day" +%m)

# date yoy
#prev_day=$(date -d "$cur_date -1 days" +%Y-%m-%d)
#fd_year=$(date -d "$prev_day" +%Y-01-01)

# JAN MOM & YOY
/home/vjavier/scripts/newbi/prelive/gr_01_01_11.sh
/home/vjavier/scripts/newbi/prelive/gr_01_01_21.sh
/home/vjavier/scripts/newbi/prelive/gr_01_01_31.sh

 # FEB MOM & YOY
/home/vjavier/scripts/newbi/prelive/gr_02_01_11.sh
/home/vjavier/scripts/newbi/prelive/gr_02_01_21.sh
/home/vjavier/scripts/newbi/prelive/gr_02_01_28.sh

 # MARCH MOM & YOY
/home/vjavier/scripts/newbi/prelive/gr_03_01_11.sh
/home/vjavier/scripts/newbi/prelive/gr_03_01_21.sh
/home/vjavier/scripts/newbi/prelive/gr_03_01_31.sh

 # APR MOM & YOY
/home/vjavier/scripts/newbi/prelive/gr_04_01_11.sh
/home/vjavier/scripts/newbi/prelive/gr_04_01_21.sh
/home/vjavier/scripts/newbi/prelive/gr_04_01_30.sh

 # MAY MOM & YOY
/home/vjavier/scripts/newbi/prelive/gr_05_01_11.sh
/home/vjavier/scripts/newbi/prelive/gr_05_01_21.sh
/home/vjavier/scripts/newbi/prelive/gr_05_01_31.sh

 # JUN MOM & YOY
/home/vjavier/scripts/newbi/prelive/gr_06_01_11.sh
/home/vjavier/scripts/newbi/prelive/gr_06_01_21.sh
