require 'aws-sdk-s3'
require 'json'

Aws.use_bundled_cert!

#s3 = Aws::S3::Client.new(profile: 'biuser@imonggo.com', region: 'ap-southeast-1')

# Get a list of buckets
# resp = s3.list_buckets
# resp.buckets.each do |bucket|
#   puts bucket.name
# end

# Create bucket
# s3.create_bucket(bucket: "testfromkikoscomputer")

puts "Initiated file upload to S3"
puts ""

s3 = Aws::S3::Resource.new

files = Dir.glob('./*.tsv.gz')
files.each do |file|
	file = file[2..-1]
	bucketName = file.split('-')

	puts file
	# Put file in bucket
	# sales files

	# ac_gr_mom
	if bucketName[0].downcase == 'ac_gr_mom'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-ac-gr-mom"
		puts ""
		s3.bucket('771542467249-jfc-executive-ac-gr-mom').object(file).upload_file(file)
	# ac_gr_yoy
  elsif bucketName[0].downcase == 'ac_gr_yoy'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-ac-gr-yoy"
		puts ""
		s3.bucket('771542467249-jfc-executive-ac-gr-yoy').object(file).upload_file(file)
	# pm_gr_mom
	elsif bucketName[0].downcase == 'pm_gr_mom'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-pm-gr-mom"
		puts ""
		s3.bucket('771542467249-jfc-executive-pm-gr-mom').object(file).upload_file(file)
	# pm_gr_yoy
	elsif bucketName[0].downcase == 'pm_gr_yoy'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-pm-gr-yoy"
		puts ""
		s3.bucket('771542467249-jfc-executive-pm-gr-yoy').object(file).upload_file(file)
	# pullout_gr_mom
	elsif bucketName[0].downcase == 'pullout_gr_mom'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-po-gr-mom"
		puts ""
		s3.bucket('771542467249-jfc-executive-po-gr-mom').object(file).upload_file(file)
	# pullout_gr_yoy
	elsif bucketName[0].downcase == 'pullout_gr_yoy'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-po-gr-yoy"
		puts ""
		s3.bucket('771542467249-jfc-executive-po-gr-yoy').object(file).upload_file(file)
	# sales_gr_mom
	elsif bucketName[0].downcase == 'sales_gr_mom'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-sales-gr-mom"
		puts ""
		s3.bucket('771542467249-jfc-executive-sales-gr-mom').object(file).upload_file(file)
	# sales_gr_yoy
  elsif bucketName[0].downcase == 'sales_gr_yoy'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-sales-gr-yoy"
		puts ""
		s3.bucket('771542467249-jfc-executive-sales-gr-yoy').object(file).upload_file(file)
	# tc_gr_mom
	elsif bucketName[0].downcase == 'tc_gr_mom'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-tc-gr-mom"
		puts ""
		s3.bucket('771542467249-jfc-executive-tc-gr-mom').object(file).upload_file(file)
	# tc_gr_yoy
	elsif bucketName[0].downcase == 'tc_gr_yoy'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-tc-gr-yoy"
		puts ""
		s3.bucket('771542467249-jfc-executive-tc-gr-yoy').object(file).upload_file(file)
	end
end

puts "Uploaded files to S3!"
puts ""
