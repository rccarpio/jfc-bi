require 'aws-sdk-s3'
require 'json'

Aws.use_bundled_cert!

#s3 = Aws::S3::Client.new(profile: 'biuser@imonggo.com', region: 'ap-southeast-1')

# Get a list of buckets
# resp = s3.list_buckets
# resp.buckets.each do |bucket|
#   puts bucket.name
# end

# Create bucket
# s3.create_bucket(bucket: "testfromkikoscomputer")

puts "Initiated file upload to S3"
puts ""

s3 = Aws::S3::Resource.new

files = Dir.glob('./*.tsv.gz')
files.each do |file|
	file = file[2..-1]
	bucketName = file.split('-')

	puts file
	# Put file in bucket
	# sales files
	if bucketName[0].downcase == 'sales_tabular'
		puts "+-----------+"
		puts "| Inserting |"
		puts "+-----------+"
		puts "File: #{file}"
		puts "Bucket: 771542467249-jfc-executive-sales-tabular"
		puts ""
		s3.bucket('771542467249-jfc-executive-sales-tabular').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'sales_bread_tabular'
		puts "+-----------+"
        puts "| Inserting |"
        puts "+-----------+"
        puts "File: #{file}"
        puts "Bucket: 771542467249-jfc-executive-sales-bread-tabular"
        puts ""
        s3.bucket('771542467249-jfc-executive-sales-bread-tabular').object(file).upload_file(file)
    elsif bucketName[0].downcase == 'sales_nonbread_tabular'
		puts "+-----------+"
        puts "| Inserting |"
        puts "+-----------+"
        puts "File: #{file}"
        puts "Bucket: 771542467249-jfc-executive-sales-nonbread-tabular"
        puts ""
        s3.bucket('771542467249-jfc-executive-sales-nonbread-tabular').object(file).upload_file(file)
    elsif bucketName[0].downcase == 'sales_peak'
		puts "+-----------+"
        puts "| Inserting |"
        puts "+-----------+"
        puts "File: #{file}"
        puts "Bucket: 771542467249-jfc-executive-sales-peak"
        puts ""
        s3.bucket('771542467249-jfc-executive-sales-peak').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'sales_peak_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-sales-peak-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-sales-peak-tabular').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'sales_catchment_sqm_man_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-sales-catchment-sqm"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-sales-catchment-sqm').object(file).upload_file(file)
	#pm files
	elsif bucketName[0].downcase == 'pm_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-pm-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-pm-tabular').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'pm_bread_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-pm-bread-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-pm-bread-tabular').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'pm_nonbread_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-pm-nonbread-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-pm-nonbread-tabular').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'pm_catchment_sqm_man_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-pm-catchment-sqm-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-pm-catchment-sqm-tabular').object(file).upload_file(file)
	#pareto files
	elsif bucketName[0].downcase == 'pareto_regions_1' || bucketName[0].downcase == 'pareto_regions_2'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-sales-pareto-tabular-nationwide"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-sales-pareto-tabular-nationwide').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'pareto_districts'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-sales-pareto-districtwide"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-sales-pareto-districtwide').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'pareto_branches'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-sales-pareto-tabular-districtwide"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-sales-pareto-tabular-districtwide').object(file).upload_file(file)
	#tc files
	elsif bucketName[0].downcase == 'tc_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-tc-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-tc-tabular').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'tc_peak'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-tc-peak"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-tc-peak').object(file).upload_file(file)
	elsif bucketName[0].downcase == 'tc_peak_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-tc-peak-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-tc-peak-tabular').object(file).upload_file(file)
	#ac files
	elsif bucketName[0].downcase == 'ac_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-ac-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-ac-tabular').object(file).upload_file(file)
	#pullout files
	elsif bucketName[0].downcase == 'pullout_tabular'
		puts "+-----------+"
	    puts "| Inserting |"
	    puts "+-----------+"
	    puts "File: #{file}"
	    puts "Bucket: 771542467249-jfc-executive-po-tabular"
	    puts ""
	    s3.bucket('771542467249-jfc-executive-po-tabular').object(file).upload_file(file)
	end
end

puts "Uploaded files to S3!"
puts ""
