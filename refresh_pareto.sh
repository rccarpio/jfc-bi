#!/bin/bash
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

echo "Refreshing Pareto"
echo ""

#test
touch /var/lib/mysql-files/test.txt

#get dates
mtdstart=`date -d "$(date +%Y-%m-01)" +%Y-%m-%d`
ytdstart=`date -d "$(date +%Y-01-01)" +%Y-%m-%d`
mtdend=`date --date="today" +%Y-%m-%d`
ytdend=`date --date="today" +%Y-%m-%d`
datestamp=`date +%Y-%m-%d`

#pareto_nationwide
echo "Downloading pareto_nationwide data..."
mysql -uroot -pStarbucks jfc_production -e "set @rank1=1, @cumulative1=0, @rank2=1, @cumulative2 = 0, @cur_reg1 = '', @cur_reg2 = ''; select IF(prod_sales.reg = 3,'LUZ',IF(prod_sales.reg = 4,'VIS',IF(prod_sales.reg = 5,'MIN',''))) as reg, 'MTD', p.name, p.stock_no, c.name, s.name, prod_sales.checker, prod_sales.rank, prod_sales.checker2, prod_sales.to_s, prod_sales.cumulative from (select tot.reg as reg, top.product as product, if(@cur_reg1 = top.reg, @rank1 := @rank1+1, @rank1 := 1) as rank, if(@rank1 = 1, @cumulative1 := 0, '') as checker, if(@cur_reg1 = top.reg, @cur_reg1 := @cur_reg1, @cur_reg1 := top.reg) as checker2, top.subtotal/tot.amount as to_s, @cumulative1 := @cumulative1 + ifnull((top.subtotal/tot.amount),0) as cumulative from (select i.account_id as reg, sum(i.amount) as amount from invoices i where i.business_date >= '$mtdstart' and i.business_date < '$mtdend' and i.status = 'S' and (i.account_id = 3 or i.account_id = 4 or i.account_id = 5) group by i.account_id)tot right join (select i.account_id as reg, il.product_id as product, sum(il.subtotal) as subtotal from invoices i inner join invoice_lines il on i.id = il.invoice_id where i.business_date >= '$mtdstart' and i.business_date < '$mtdend' and i.status = 'S' and (i.account_id = 3 or i.account_id = 4 or i.account_id = 5) group by i.account_id, il.product_id order by i.account_id, sum(il.subtotal) desc)top on tot.reg = top.reg)prod_sales inner join products p on p.id = prod_sales.product left join categories c on c.id = p.category_id left join subcategories s on s.id = p.subcategory_id where prod_sales.cumulative <= 0.80 order by prod_sales.reg, prod_sales.rank asc INTO outfile '/var/lib/mysql-files/pareto_regions_1.tsv' fields terminated by '\t' lines terminated by '\n';"
mysql -uroot -pStarbucks jfc_production -e "set @rank1=1, @cumulative1=0, @rank2=1, @cumulative2 = 0, @cur_reg1 = '', @cur_reg2 = ''; select IF(prod_sales.reg = 3,'LUZ',IF(prod_sales.reg = 4,'VIS',IF(prod_sales.reg = 5,'MIN',''))) as reg, 'YTD', p.name, p.stock_no, c.name, s.name, prod_sales.checker, prod_sales.rank, prod_sales.checker2, prod_sales.to_s, prod_sales.cumulative from (select tot.reg as reg, top.product as product, if(@cur_reg2 = top.reg, @rank2 := @rank2+1, @rank2 := 1) as rank, if(@rank2 = 1, @cumulative2 := 0, '') as checker, if(@cur_reg2 = top.reg, @cur_reg2 := @cur_reg2, @cur_reg2 := top.reg) as checker2, top.subtotal/tot.amount as to_s, @cumulative2 := @cumulative2 + ifnull((top.subtotal/tot.amount),0) as cumulative from (select i.account_id as reg, sum(i.amount) as amount from invoices i where i.business_date >= '$ytdstart' and i.business_date < '$ytdend' and i.status = 'S' and (i.account_id = 3 or i.account_id = 4 or i.account_id = 5) group by i.account_id)tot right join (select i.account_id as reg, il.product_id as product, sum(il.subtotal) as subtotal from invoices i inner join invoice_lines il on i.id = il.invoice_id where i.business_date >= '$ytdstart' and i.business_date < '$ytdend' and i.status = 'S' and (i.account_id = 3 or i.account_id = 4 or i.account_id = 5) group by i.account_id, il.product_id order by i.account_id, sum(il.subtotal) desc)top on tot.reg = top.reg)prod_sales inner join products p on p.id = prod_sales.product left join categories c on c.id = p.category_id left join subcategories s on s.id = p.subcategory_id where prod_sales.cumulative <= 0.80 order by prod_sales.reg, prod_sales.rank asc INTO outfile '/var/lib/mysql-files/pareto_regions_2.tsv' fields terminated by '\t' lines terminated by '\n'; "
echo "pareto_nationwide downloaded!"
echo ""

#pareto_districts
echo "Downloading pareto_districts data..."
mysql -uroot -pStarbucks jfc_production -e "set @cur_br = '', @cum_sum = 0, @rank=1, @cur_br2 = '', @cum_sum2 = 0, @rank2=1; select * from ((select ovr.code, 'MTD', ovr.sku, ovr.category, ovr.subcategory, ovr.product, ovr.checker, ovr.rank, ovr.checker2, ovr.to_s, ovr.cummulative from (select prod.code as code, prod.stock_no as sku, prod.category as category, prod.subcategory as subcategory, prod.product as product, if(@cur_br = prod.code, @rank := @rank+1, @rank := 1) as rank, if(@rank = 1, @cum_sum := 0, '') as checker, if(@cur_br = prod.code, @cur_br := @cur_br, @cur_br := prod.code) as checker2, ifnull((prod.sales/tot.sales),0) as to_s, @cum_sum := @cum_sum + ifnull((prod.sales/tot.sales),0) as cummulative from (select d.name as code, p.stock_no as stock_no, c.name as category, s.name as subcategory, p.name as product, sum(il.subtotal) as sales from invoices i inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id inner join invoice_lines il on i.id = il.invoice_id inner join products p on p.id = il.product_id left join categories c on c.id = p.category_id left join subcategories s on s.id = p.subcategory_id where i.business_date >= '2018-06-01' and i.business_date < '2018-07-01' group by d.name, p.stock_no, c.name, s.name, p.name order by d.name, sum(il.subtotal) desc)prod left join ( select d.name as code, sum(i.amount) as sales from invoices i inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id where i.business_date >= '2018-06-01' and i.business_date < '2018-07-01' group by d.name )tot on prod.code=tot.code order by prod.code, ifnull((prod.sales/tot.sales),0) desc)ovr where ovr.cummulative <= 0.80 order by ovr.code, ovr.rank asc) UNION ALL (select ovr.code, 'YTD', ovr.sku, ovr.category, ovr.subcategory, ovr.product, ovr.checker, ovr.rank, ovr.checker2, ovr.to_s, ovr.cummulative from (select prod.code as code, prod.stock_no as sku, prod.category as category, prod.subcategory as subcategory, prod.product as product, if(@cur_br2 = prod.code, @rank2 := @rank2+1, @rank2 := 1) as rank, if(@rank2 = 1, @cum_sum2 := 0, '') as checker, if(@cur_br2 = prod.code, @cur_br2 := @cur_br2, @cur_br2 := prod.code) as checker2, ifnull((prod.sales/tot.sales),0) as to_s, @cum_sum2 := @cum_sum2 + ifnull((prod.sales/tot.sales),0) as cummulative from (select d.name as code, p.stock_no as stock_no, c.name as category, s.name as subcategory, p.name as product, sum(il.subtotal) as sales from invoices i inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id inner join invoice_lines il on i.id = il.invoice_id inner join products p on p.id = il.product_id left join categories c on c.id = p.category_id left join subcategories s on s.id = p.subcategory_id where i.business_date >= '2018-01-01' and i.business_date < '2018-07-01' group by d.name, p.stock_no, c.name, s.name, p.name order by d.name, sum(il.subtotal) desc)prod left join ( select d.name as code, sum(i.amount) as sales from invoices i inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id where i.business_date >= '2018-01-01' and i.business_date < '2018-07-01' group by d.name )tot on prod.code=tot.code order by prod.code, ifnull((prod.sales/tot.sales),0) desc)ovr where ovr.cummulative <= 0.80 order by ovr.code, ovr.rank asc))x INTO outfile '/var/lib/mysql-files/pareto_districts.tsv' fields terminated by '\t' lines terminated by '\n'; "
echo "pareto_districts downloaded!"
echo ""

#pareto_branches
echo "Downloading pareto_branches data..."
mysql -uroot -pStarbucks jfc_production -e "set @cur_br = '', @cum_sum = 0, @rank=1, @cur_br2 = '', @cum_sum2 = 0, @rank2=1; select * from ((select ovr.code, ovr.name, 'MTD', ovr.sku, ovr.category, ovr.subcategory, ovr.product, ovr.checker, ovr.rank, ovr.checker2, ovr.to_s, ovr.cummulative from (select prod.code as code, prod.name as name, prod.stock_no as sku, prod.category as category, prod.subcategory as subcategory, prod.product as product, if(@cur_br = prod.code, @rank := @rank+1, @rank := 1) as rank, if(@rank = 1, @cum_sum := 0, '') as checker, if(@cur_br = prod.code, @cur_br := @cur_br, @cur_br := prod.code) as checker2, ifnull((prod.sales/tot.sales),0) as to_s, @cum_sum := @cum_sum + ifnull((prod.sales/tot.sales),0) as cummulative from (select b.code as code, b.short_name as name, p.stock_no as stock_no, c.name as category, s.name as subcategory, p.name as product, sum(il.subtotal) as sales from invoices i inner join branches b on b.id = i.branch_id inner join invoice_lines il on i.id = il.invoice_id inner join products p on p.id = il.product_id left join categories c on c.id = p.category_id left join subcategories s on s.id = p.subcategory_id where i.business_date >= '2018-06-01' and i.business_date < '2018-07-01' group by b.code, b.short_name, p.stock_no, c.name, s.name, p.name order by b.code, sum(il.subtotal) desc)prod left join ( select b.code as code, sum(i.amount) as sales from invoices i inner join branches b on b.id = i.branch_id where i.business_date >= '2018-06-01' and i.business_date < '2018-07-01' group by b.code )tot on prod.code=tot.code order by prod.code, rank asc)ovr where ovr.cummulative <= 0.80 order by ovr.rank asc) UNION (select ovr.code, ovr.name, 'YTD', ovr.sku, ovr.category, ovr.subcategory, ovr.product, ovr.checker, ovr.rank, ovr.checker2, ovr.to_s, ovr.cummulative from (select prod.code as code, prod.name as name, prod.stock_no as sku, prod.category as category, prod.subcategory as subcategory, prod.product as product, if(@cur_br2 = prod.code, @rank2 := @rank2+1, @rank2 := 1) as rank, if(@rank2 = 1, @cum_sum2 := 0, '') as checker, if(@cur_br2 = prod.code, @cur_br2 := @cur_br2, @cur_br2 := prod.code) as checker2, ifnull((prod.sales/tot.sales),0) as to_s, @cum_sum2 := @cum_sum2 + ifnull((prod.sales/tot.sales),0) as cummulative from (select b.code as code, b.short_name as name, p.stock_no as stock_no, p.name as product, c.name as category, s.name as subcategory, sum(il.subtotal) as sales from invoices i inner join branches b on b.id = i.branch_id inner join invoice_lines il on i.id = il.invoice_id inner join products p on p.id = il.product_id left join categories c on c.id = p.category_id left join subcategories s on s.id = p.subcategory_id where i.business_date >= '2018-01-01' and i.business_date < '2018-07-01' group by b.code, b.short_name, p.stock_no, c.name, s.name, p.name order by b.code, sum(il.subtotal) desc)prod left join ( select b.code as code, sum(i.amount) as sales from invoices i inner join branches b on b.id = i.branch_id where i.business_date >= '2018-01-01' and i.business_date < '2018-07-01' group by b.code )tot on prod.code=tot.code order by prod.code, rank asc)ovr where ovr.cummulative <= 0.80 order by ovr.rank asc))x INTO outfile '/var/lib/mysql-files/pareto_branches.tsv' fields terminated by '\t' lines terminated by '\n'; "
echo "pareto_branches downloaded!"
echo ""

#go to var lib
cd /var/lib/mysql-files

#move files to shared/bi
echo "Moving files..."
mv pareto_regions_1.tsv /home/vjavier/exports/shared/bi/newbi
mv pareto_regions_2.tsv /home/vjavier/exports/shared/bi/newbi
mv pareto_districts.tsv /home/vjavier/exports/shared/bi/newbi
mv pareto_branches.tsv /home/vjavier/exports/shared/bi/newbi
echo "Files moved to /home/vjavier/exports/shared/bi/newbi"
echo ""

#compress the files
echo "Compressing files..."
cd /home/vjavier/exports/shared/bi/newbi
gzip pareto_regions_1.tsv
gzip pareto_regions_2.tsv
gzip pareto_districts.tsv
gzip pareto_branches.tsv
echo "Files compressed!"
echo ""

#rename the files with the datestamp
echo "Renaming files..."
mv pareto_regions_1.tsv.gz pareto_regions_1-$datestamp.tsv.gz
mv pareto_regions_2.tsv.gz pareto_regions_2-$datestamp.tsv.gz
mv pareto_districts.tsv.gz pareto_districts-$datestamp.tsv.gz
mv pareto_branches.tsv.gz pareto_branches-$datestamp.tsv.gz
echo "Files renamed!"
echo ""