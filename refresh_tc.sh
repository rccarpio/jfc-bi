#!/bin/bash
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

echo "Refreshing TC"
echo ""

#test
touch /var/lib/mysql-files/test.txt

#get dates
today=`date --date="yesterday" +%Y-%m-%d`
yest=`date --date="2 days ago" +%Y-%m-%d`
mtdstart=`date -d "$(date +%Y-%m-01)" +%Y-%m-%d`
ytdstart=`date -d "$(date +%Y-01-01)" +%Y-%m-%d`
mtdend=`date --date="today" +%Y-%m-%d`
ytdend=`date --date="today" +%Y-%m-%d`
datestamp=`date +%Y-%m-%d`

#tc_tabular
echo "Downloading tc_tabular data..."
mysql -uroot -pStarbucks jfc_production -e "select YEAR(i.business_date) as year, MONTH(i.business_date) as month, i.business_date as date, IF(b.account_id = 3,'LUZ',IF(b.account_id = 4,'VIS',IF(b.account_id = 5,'MIN',''))) as region, dv.name as district, b.code as code, b.short_name as short_name, count(i.id) as transaction_count from invoices i inner join branches b on b.id = i.branch_id inner join divisions dv on b.division_id = dv.id where i.updated_at >= '$yest 16:00:00' and i.updated_at < '$today 16:00:00' and i.status = 'S' group by year, month, date, region, district, code, short_name INTO outfile '/var/lib/mysql-files/tc_tabular.tsv' fields terminated by '\t' lines terminated by '\n';"
echo "tc_tabular downloaded!"
echo ""

#tc_peak
echo "Downloading tc_peak data..."
mysql -uroot -pStarbucks jfc_production -e "SELECT i.business_date as business_date, DAYOFWEEK(i.business_date) as day_of_week, HOUR(ADDTIME(i.invoice_date,'08:00:00')) as hour, IF(b.account_id = 3,'LUZ',IF(b.account_id = 4,'VIS',IF(b.account_id = 5,'MIN',''))) as region, d.name as district, b.code as branch_code, b.short_name as branch_name, count(i.id) as transaction_count FROM invoices i inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id WHERE i.updated_at >= '$yest 16:00:00' and i.updated_at < '$today 16:00:00' and i.status = 'S' GROUP BY i.business_date, DAYOFWEEK(i.business_date), HOUR(ADDTIME(i.invoice_date,'08:00:00')), region, d.name, b.code, b.short_name INTO outfile '/var/lib/mysql-files/tc_peak.tsv' fields terminated by '\t' lines terminated by '\n';"
echo "tc_peak downloaded!"
echo ""

#tc_peak_tabular
echo "Downloading tc_peak_tabular data..."
mysql -uroot -pStarbucks jfc_production -e "Set @rank=1, @branch='', @rank2=1, @branch2=''; select tot.region, tot.district, tot.code, tot.name, tot.amount, 'MTD', peak_hour.hour, peak_hour.amount/hourdiv.divisor, peak_day.day, peak_day.amount/daydiv.divisor, daydiv.divisor, hourdiv.divisor from (select substring(a.name,4,3) as region, d.name as district, b.code as code, b.short_name as name, sum(i.amount) as amount from invoices i inner join accounts a on a.id = i.account_id inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id where i.status = 'S' and i.business_date >= '2018-06-01' and i.business_date < '2018-07-01' group by region, district, name, code)tot inner join (select region, district, code, name, branch_id, day, amount as amount from (Select day.region as region, day.district as district, day.branch_code as code, day.name as name, day.branch_id as branch_id, day.peak_day_of_week as day, day.amount as amount, if(@branch = day.branch_code, @rank := @rank+1, @rank := 1) as rank, if(@branch = day.branch_code,'',@branch := day.branch_code) from (Select substring(a.name,4,3) as region, d.name as district, b.code as branch_code, b.short_name as name, b.id as branch_id, DAYOFWEEK(i.business_date) as peak_day_of_week, count(i.id) as amount from invoices i inner join accounts a on a.id = i.account_id inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id where i.status = 'S' and i.business_date >= '2018-06-01' and i.business_date < '2018-07-01' group by region, district, branch_code, name, branch_id, peak_day_of_week order by branch_code, amount desc)day group by region, district, code, name, branch_id, day )x where x.rank = 1 order by region, district, code)peak_day on tot.region = peak_day.region and tot.district = peak_day.district and tot.code = peak_day.code inner join (select region, district, code, name, branch_id, hour, amount as amount from (Select hour.region as region, hour.district as district, hour.branch_code as code, hour.name as name, hour.branch_id as branch_id, hour.peak_hour as hour, hour.amount as amount, if(@branch = hour.branch_code, @rank := @rank+1, @rank := 1) as rank, if(@branch = hour.branch_code,'',@branch := hour.branch_code) from (Select substring(a.name,4,3) as region, d.name as district, b.code as branch_code, b.short_name as name, b.id as branch_id, HOUR(ADDTIME(i.invoice_date,'08:00:00')) as peak_hour, count(i.id) as amount from invoices i inner join accounts a on a.id = i.account_id inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id where i.status = 'S' and i.business_date >= '2018-06-01' and i.business_date < '2018-07-01' group by region, district, branch_code, name, branch_id, peak_hour order by branch_code, amount desc)hour group by region, district, code, name, branch_id, hour )x where x.rank = 1 order by region, district, code)peak_hour on peak_hour.region = peak_day.region and peak_hour.district = peak_day.district and peak_hour.code = peak_day.code inner join (select branch_id as branch_id, dayofweek(business_date) as dayofweek, count(distinct business_date) as divisor from invoices where business_date >= '2018-06-01' and business_date < '2018-07-01' group by branch_id, dayofweek(business_date))daydiv on peak_day.branch_id = daydiv.branch_id and peak_day.day = daydiv.dayofweek inner join (select branch_id as branch_id, count(distinct business_date) as divisor from invoices where business_date >= '2018-06-01' and business_date < '2018-07-01' group by branch_id)hourdiv on peak_hour.branch_id = hourdiv.branch_id UNION select tot.region, tot.district, tot.code, tot.name, tot.amount, 'YTD', peak_hour.hour, peak_hour.amount/hourdiv.divisor, peak_day.day, peak_day.amount/daydiv.divisor, daydiv.divisor, hourdiv.divisor from (select substring(a.name,4,3) as region, d.name as district, b.code as code, b.short_name as name, sum(i.amount) as amount from invoices i inner join accounts a on a.id = i.account_id inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id where i.status = 'S' and i.business_date >= '2018-01-01' and i.business_date < '2018-07-01' group by region, district, name, code)tot inner join (select region, district, code, name, branch_id, day, amount as amount from (Select day.region as region, day.district as district, day.branch_code as code, day.name as name, day.branch_id as branch_id, day.peak_day_of_week as day, day.amount as amount, if(@branch = day.branch_code, @rank := @rank+1, @rank := 1) as rank, if(@branch = day.branch_code,'',@branch := day.branch_code) from (Select substring(a.name,4,3) as region, d.name as district, b.code as branch_code, b.short_name as name, b.id as branch_id, DAYOFWEEK(i.business_date) as peak_day_of_week, count(i.id) as amount from invoices i inner join accounts a on a.id = i.account_id inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id where i.status = 'S' and i.business_date >= '2018-01-01' and i.business_date < '2018-07-01' group by region, district, branch_code, name, branch_id, peak_day_of_week order by branch_code, amount desc)day group by region, district, code, name, branch_id, day )x where x.rank = 1 order by region, district, code)peak_day on tot.region = peak_day.region and tot.district = peak_day.district and tot.code = peak_day.code inner join (select region, district, code, name, branch_id, hour, amount as amount from (Select hour.region as region, hour.district as district, hour.branch_code as code, hour.name as name, hour.branch_id as branch_id, hour.peak_hour as hour, hour.amount as amount, if(@branch = hour.branch_code, @rank := @rank+1, @rank := 1) as rank, if(@branch = hour.branch_code,'',@branch := hour.branch_code) from (Select substring(a.name,4,3) as region, d.name as district, b.code as branch_code, b.short_name as name, b.id as branch_id, HOUR(ADDTIME(i.invoice_date,'08:00:00')) as peak_hour, count(i.id) as amount from invoices i inner join accounts a on a.id = i.account_id inner join branches b on b.id = i.branch_id inner join divisions d on d.id = b.division_id where i.status = 'S' and i.business_date >= '2018-01-01' and i.business_date < '2018-07-01' group by region, district, branch_code, name, branch_id, peak_hour order by branch_code, amount desc)hour group by region, district, code, name, branch_id, hour )x where x.rank = 1 order by region, district, code)peak_hour on peak_hour.region = peak_day.region and peak_hour.district = peak_day.district and peak_hour.code = peak_day.code inner join (select branch_id as branch_id, dayofweek(business_date) as dayofweek, count(distinct business_date) as divisor from invoices where business_date >= '2018-01-01' and business_date < '2018-07-01' group by branch_id, dayofweek(business_date))daydiv on peak_day.branch_id = daydiv.branch_id and peak_day.day = daydiv.dayofweek inner join (select branch_id as branch_id, count(distinct business_date) as divisor from invoices where business_date >= '2018-01-01' and business_date < '2018-07-01' group by branch_id)hourdiv on peak_hour.branch_id = hourdiv.branch_id INTO outfile '/var/lib/mysql-files/tc_peak_tabular.tsv' fields terminated by '\t' lines terminated by '\n';"
echo "tc_peak_tabular downloaded!"
echo ""

#go to var lib
cd /var/lib/mysql-files

echo "Moving files..."
#move files to shared/bi
mv tc_tabular.tsv /home/vjavier/exports/shared/bi/newbi
mv tc_peak.tsv /home/vjavier/exports/shared/bi/newbi
mv tc_peak_tabular.tsv /home/vjavier/exports/shared/bi/newbi
echo "Files moved to /home/vjavier/exports/shared/bi/newbi"
echo ""

#compress the files
echo "Compressing files..."
cd /home/vjavier/exports/shared/bi/newbi
gzip tc_tabular.tsv
gzip tc_peak.tsv
gzip tc_peak_tabular.tsv
echo "Files compressed!"
echo ""

#rename the files with the datestamp
echo "Renaming Files..."
mv tc_tabular.tsv.gz tc_tabular-$datestamp.tsv.gz
mv tc_peak.tsv.gz tc_peak-$datestamp.tsv.gz
mv tc_peak_tabular.tsv.gz tc_peak_tabular-$datestamp.tsv.gz
echo "Files renamed!"
echo ""