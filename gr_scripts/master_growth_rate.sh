#!/bin/bash
source /home/vjavier/.rvm/environments/ruby-2.3.3
cd /home/vjavier/scripts/newbi/gr_scripts
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
rvm use ruby-2.3.3

#aws sdk shiz
export AWS_ACCESS_KEY_ID=AKIAISWW45RZ5XGO3AQQ
export AWS_SECRET_ACCESS_KEY=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
export AWS_REGION=ap-southeast-1

#global vars
current_date=`date +%Y-%m-%d`
tmp_startdate=''
if [ "`date +%d --date=$current_date`" = "01" ]
then
  tmp_startdate=`date +%Y-%m-%d --date="$current_date 1 month ago"`
else
  tmp_startdate=`date +%Y-%m-%d --date="$current_date 10 days ago"`
fi
current_month=`date +%m --date=$tmp_startdate`

startdate_mom=`date +%Y-%m-01 --date=$tmp_startdate`
enddate_mom=`date +%Y-%m-%d --date="$current_date 1 day ago"`

startdate_yoy=`date +%Y-01-01 --date=$tmp_startdate`
enddate_yoy=`date +%Y-%m-%d --date="$current_date 1 day ago"`

export filename_mom=$current_month"-"`date +%Y%m%d --date=$startdate_mom`"-"`date +%Y%m%d --date=$enddate_mom`
export filename_yoy=$current_month"-"`date +%Y%m%d --date=$startdate_yoy`"-"`date +%Y%m%d --date=$enddate_yoy`
#

#run ac gr refresh script
/home/vjavier/scripts/newbi/gr_scripts/refresh_gr_ac.sh

#run pm gr refresh script
/home/vjavier/scripts/newbi/gr_scripts/refresh_gr_pm.sh

#run po gr refresh script
/home/vjavier/scripts/newbi/gr_scripts/refresh_gr_po.sh

#run sales gr refresh script
/home/vjavier/scripts/newbi/gr_scripts/refresh_gr_sales.sh

#run tc gr refresh script
/home/vjavier/scripts/newbi/gr_scripts/refresh_gr_tc.sh

#put files to s3
#cd /home/vjavier/exports/shared/bi/newbi/gr_scripts
#ruby refresh_growth_rate_files.rb
