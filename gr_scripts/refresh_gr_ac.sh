#!/bin/bash
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

title='Average Check'
full_fn_mom="ac_gr_mom-$filename_mom"
full_fn_yoy="ac_gr_yoy-$filename_yoy"

echo "Refreshing $title Growth Rate"
echo ""

#

#GR_MOM
outfile_mom="INTO outfile '/var/lib/mysql-files/$full_fn_mom.tsv' fields terminated by '\t' lines terminated by '\n';"
echo "Downloading $filename_mom data..."
mysql -uroot -pStarbucks jfc_production -e "set @tmp_date = now(); set @fd_month = date_format(@tmp_date, '%Y-%m-01'); set @cur_date = date_format(@tmp_date, '%Y-%m-%d'); set @cur_day = date_format(@tmp_date, '%d'); set @ld_month = last_day(if(@cur_day = 01,date_sub(@cur_date,interval 1 month),@cur_date)); set @start_date = date_format(if(@cur_day = 01 or @cur_day = 1, date_sub(@tmp_date, interval 1 month), @tmp_date), '%Y-%m-01'); set @tmp_end_date = date_format(if(@cur_day = 01 or @cur_day = 1, last_day(date_sub(@tmp_date, interval 1 month)),@cur_date),'%Y-%m-%d'); set @end_date = if(date_format(@tmp_end_date, '%d') in (28,29,30,31),date_add(@tmp_end_date, interval 1 day),@tmp_end_date); set @year = year(date_sub(@end_date, interval 1 day)); set @month = month(date_sub(@end_date, interval 1 day)); set @date = date(date_sub(@end_date, interval 1 day)); set @date_range = concat(date_format(@start_date,'%m-%d'),' to ',date_format(@date,'%m-%d')); select cur.year, cur.month, cur.date_range, cur.date, cur.region, cur.district, cur.code, cur.short_name, cur.average_check, (cur.average_check - prev.average_check)/cur.average_check from (select @year as year, @month as month, @date_range as date_range, @date as date, IF(b.account_id = 3,'LUZ',IF(b.account_id = 4,'VIS',IF(b.account_id = 5,'MIN',''))) as region, dv.name as district, b.code as code, b.short_name as short_name, avg(i.amount) as average_check from invoices i inner join branches b on b.id = i.branch_id inner join divisions dv on b.division_id = dv.id where i.business_date >= @start_date and i.business_date < @end_date and i.status = 'S' group by year, month, date, region, district, code, short_name)cur left join (select YEAR(i.business_date) as year, MONTH(i.business_date) as month, i.business_date as date, IF(b.account_id = 3,'LUZ',IF(b.account_id = 4,'VIS',IF(b.account_id = 5,'MIN',''))) as region, dv.name as district, b.code as code, b.short_name as short_name, avg(i.amount) as average_check from invoices i inner join branches b on b.id = i.branch_id inner join divisions dv on b.division_id = dv.id where i.business_date >= DATE_SUB(@start_date, INTERVAL 1 MONTH) and i.business_date < DATE_SUB(@end_date, INTERVAL 1 MONTH) and i.status = 'S' group by year, month, date, region, district, code, short_name )prev on cur.region = prev.region and cur.district = prev.district and cur.code = prev.code and cur.short_name = prev.short_name where (cur.code <> '') $outfile_mom"
echo "$title $filename_mom downloaded!"
echo ""

#GR_YOY
outfile_yoy="INTO outfile '/var/lib/mysql-files/$full_fn_yoy.tsv' fields terminated by '\t' lines terminated by '\n';"
echo "Downloading $filename_yoy data..."
mysql -uroot -pStarbucks jfc_production -e "set @tmp_date = now(); set @fd_year = date_format(@tmp_date, '%Y-01-01'); set @cur_date = date_format(@tmp_date, '%Y-%m-%d'); set @cur_day = date_format(@tmp_date, '%d'); set @ld_month = last_day(if(@cur_day = 01,date_sub(@cur_date,interval 1 month),@cur_date)); set @start_date = @fd_year; set @tmp_end_date = date_format(if(@cur_day = 01 or @cur_day = 1, last_day(date_sub(@tmp_date, interval 1 month)),@cur_date),'%Y-%m-%d'); set @end_date = if(date_format(@tmp_end_date, '%d') in (28,29,30,31),date_add(@tmp_end_date, interval 1 day),@tmp_end_date); set @year = year(date_sub(@end_date, interval 1 day)); set @month = month(date_sub(@end_date, interval 1 day)); set @date = date(date_sub(@end_date, interval 1 day)); set @date_range = concat(date_format(@start_date,'%m-%d'),' to ',date_format(@date,'%m-%d')); select cur.year, cur.month, cur.date_range, cur.date, cur.region, cur.district, cur.code, cur.short_name, cur.average_check, (cur.average_check - ifnull(prev.average_check,0))/cur.average_check as average_check_gr_mom from (select @year as year, @month as month, @date_range as date_range, @date as date, IF(b.account_id = 3,'LUZ',IF(b.account_id = 4,'VIS',IF(b.account_id = 5,'MIN',''))) as region, dv.name as district, b.code as code, b.short_name as short_name, avg(i.amount) as average_check from invoices i inner join branches b on b.id = i.branch_id inner join divisions dv on b.division_id = dv.id where i.business_date >= @start_date and i.business_date < @end_date and i.status = 'S' group by year, month, date, region, district, code, short_name)cur left join (select YEAR(i.business_date) as year, MONTH(i.business_date) as month, DAY(i.business_date) as day, i.business_date as date, IF(b.account_id = 3,'LUZ',IF(b.account_id = 4,'VIS',IF(b.account_id = 5,'MIN',''))) as region, dv.name as district, b.code as code, b.short_name as short_name, avg(i.amount) as average_check from invoices i inner join branches b on b.id = i.branch_id inner join divisions dv on b.division_id = dv.id where i.business_date >= DATE_SUB(@start_date, INTERVAL 1 YEAR) and i.business_date < DATE_SUB(@end_date, INTERVAL 1 YEAR) and i.status = 'S' group by year, month, date, region, district, code, short_name )prev on cur.region = prev.region and cur.district = prev.district and cur.code = prev.code and cur.short_name = prev.short_name where (cur.code <> '') $outfile_yoy"
echo "$title $filename_yoy downloaded!"
echo ""

#go to var lib
cd /var/lib/mysql-files

#move files to shared/bi/newbi/gr_scripts
echo "$title Moving files..."
mv $full_fn_mom.tsv /home/vjavier/exports/shared/bi/newbi/gr_scripts
mv $full_fn_yoy.tsv /home/vjavier/exports/shared/bi/newbi/gr_scripts
echo "$title Files moved to /home/vjavier/exports/shared/bi/newbi/gr_scripts"
echo ""

#compress the files
echo "$title Compressing files..."
cd /home/vjavier/exports/shared/bi/newbi/gr_scripts
gzip $full_fn_mom.tsv
gzip $full_fn_yoy.tsv
echo "$title Files compressed!"

#rename the files with the datestamp
datestamp=`date +%Y%m%d`
echo "$title Renaming files..."
mv $full_fn_mom.tsv.gz $full_fn_mom-$datestamp.tsv.gz
mv $full_fn_yoy.tsv.gz $full_fn_yoy-$datestamp.tsv.gz
echo "$title Files renamed!"
