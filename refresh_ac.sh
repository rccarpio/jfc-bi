#!/bin/bash
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

echo "Refreshing AC"
echo ""

#test
touch /var/lib/mysql-files/test.txt

#get dates
today=`date --date="yesterday" +%Y-%m-%d`
yest=`date --date="2 days ago" +%Y-%m-%d`
datestamp=`date +%Y-%m-%d`

#ac_tabular
echo "Downloading ac_tabular data..."
mysql -uroot -pStarbucks jfc_production -e "select YEAR(i.business_date) as year, MONTH(i.business_date) as month, i.business_date as date, IF(b.account_id = 3,'LUZ',IF(b.account_id = 4,'VIS',IF(b.account_id = 5,'MIN',''))) as region, dv.name as district, b.code as code, b.short_name as short_name, avg(i.amount) as average_check from invoices i inner join branches b on b.id = i.branch_id inner join divisions dv on b.division_id = dv.id where i.updated_at >= '$yest 16:00:00' and i.updated_at < '$today 16:00:00' and i.status = 'S' group by year, month, date, region, district, code, short_name INTO outfile '/var/lib/mysql-files/ac_tabular.tsv' fields terminated by '\t' lines terminated by '\n';"
echo "ac_tabular downloaded!"
echo ""

#go to var lib
cd /var/lib/mysql-files

#move files to shared/bi
echo "Moving files..."
mv ac_tabular.tsv /home/vjavier/exports/shared/bi/newbi
echo "Files moved to /home/vjavier/exports/shared/bi/newbi"
echo ""

#compress the files
echo "Compressing files..."
cd /home/vjavier/exports/shared/bi/newbi
gzip ac_tabular.tsv
echo "Files compressed!"

#rename the files with the datestamp
echo "Renaming files..."
mv ac_tabular.tsv.gz ac_tabular-$datestamp.tsv.gz
echo "Files renamed!"