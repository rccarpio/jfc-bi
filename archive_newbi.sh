#!/bin/sh
datestamp=`date --date="yesterday" +%Y-%m-%d`
cd /home/vjavier/exports/shared/bi/newbi/

#move all files to archive
mv *.tsv.gz archive

#rename log file
cd /home/vjavier/scripts/newbi/
mv refresh.log refresh_$datestamp.log

#move log file to logs archive
mv refresh_$datestamp.log logs
