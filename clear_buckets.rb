require 'aws-sdk-s3'
require 'json'

Aws.use_bundled_cert!

#s3 = Aws::S3::Client.new(profile: 'biuser@imonggo.com', region: 'ap-southeast-1')

# Get a list of buckets
# resp = s3.list_buckets
# resp.buckets.each do |bucket|
#   puts bucket.name
# end

# Create bucket
# s3.create_bucket(bucket: "testfromkikoscomputer")

s3 = Aws::S3::Resource.new

s3.bucket('771542467249-jfc-executive-sales-catchment-sqm').clear!
puts "771542467249-jfc-executive-sales-catchment-sqm clear!"

s3.bucket('771542467249-jfc-executive-sales-peak-tabular').clear!
puts "771542467249-jfc-executive-sales-peak-tabular clear!"

s3.bucket('771542467249-jfc-executive-sales-pareto-tabular-nationwide').clear!
puts "771542467249-jfc-executive-sales-pareto-tabular-nationwide clear!"

s3.bucket('771542467249-jfc-executive-sales-pareto-tabular-branchwide').clear!
puts "771542467249-jfc-executive-sales-pareto-tabular-branchwide clear!"

s3.bucket('771542467249-jfc-executive-sales-pareto-districtwide').clear!
puts "771542467249-jfc-executive-sales-pareto-districtwide clear!"

s3.bucket('771542467249-jfc-executive-tc-peak-tabular').clear!
puts "771542467249-jfc-executive-tc-peak-tabular clear!"

s3.bucket('771542467249-jfc-executive-pm-catchment-sqm-tabular').clear!
puts "771542467249-jfc-executive-pm-catchment-sqm-tabular clear!"

puts "All buckets cleared!"
puts ""
